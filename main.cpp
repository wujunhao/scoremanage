#include <iostream>
#include <list>
#include <fstream>
#include <vector>
using namespace std;

/**

这个项目是一个学生管理系统，在闲鱼卖了330，用时共计
下午两点30--晚上10.30 

*/ 
class Course{
private:
    string m_courseId;                      // 璇剧▼缂栧彿锛�5浣嶆暟瀛�
    string m_courseName;                    // 璇剧▼鍚嶇О
    int m_credits;
    int m_score;
public:
    Course(string courseId, string courseName, int credits, int score){
        m_courseId = courseId;
        m_courseName = courseName;
        m_credits = credits;
        m_score = score;
    }
    string getCourseId(){
        return m_courseId;
    }
    string getCourseName(){
        return m_courseName;
    }
    int getCredits(){
        return m_credits;
    }
    int getScore(){
        return m_score;
    }
};

class Student{
private:
    string m_stuId;                         // 瀛﹀彿锛�8浣嶆暟瀛�
    string m_stuName;                       // 瀛︾敓鍚嶅瓧
    list<Course> m_courseList;              // 杩欎釜瀛︾敓閫変簡鍝簺璇剧▼
public:
    Student(string stuId, string stuName){
        m_stuId = stuId;
        m_stuName = stuName;
    }
    Student(string stuId, string stuName, Course course){
        m_stuId = stuId;
        m_stuName = stuName;
        m_courseList.push_back(course);
    }
    string getStuId(){
        return m_stuId;
    }
    string getStuName(){
        return m_stuName;
    }
    list<Course>& getCourseList(){
        return m_courseList;
    }
    void showScore(){
        cout<<"瀛︾敓濮撳悕:"<<m_stuName<<endl;
        cout<<"瀛︾敓ID:"<<m_stuId<<endl;
        cout<<"璇ュ鐢熼�夋嫨鐨勮绋嬪涓嬫墍绀篭n";
        int totalCredits = 0;
        for(list<Course>::iterator it = m_courseList.begin(); it != m_courseList.end(); it++){
            totalCredits += it->getCredits();
            cout<<"璇剧▼鍚嶇О:"<<it->getCourseName()<<endl;
            cout<<"璇剧▼ID:"<<it->getCourseId()<<endl;
            cout<<"璇剧▼瀛﹀垎:"<<it->getCredits()<<endl;
            cout<<"璇剧▼鎴愮哗:"<<it->getScore()<<endl<<endl;
        }
        cout<<"璇ュ鐢熸墍淇鍒嗕负"<<totalCredits<<endl;
    }
    void appendCourse(Course course){
        m_courseList.push_back(course);
    }


    void delCourse(const string& courseId){
        for(list<Course>::iterator it = m_courseList.begin(); it != m_courseList.end(); it++){
            if(it->getCourseId() == courseId){
                m_courseList.erase(it);
                break;
            }
        }
    }


    bool isCourseValid(const string courseId){              //
        for(list<Course>::iterator it = m_courseList.begin(); it != m_courseList.end(); it++){
            if(it->getCourseId() == courseId){                 // 鏈夎繖闂ㄨ锛屽氨杩斿洖false
                return false;                               // 濡傛灉杩欎釜瀛︾敓鐨勮绋嬭〃閲屽凡缁忔湁杩欓棬璇句簡锛岃鏄庤绋嬫垚缁╂坊鍔犳棤鏁�
            }
        }
        return true;                                        // 娌℃湁杩欎竴闂ㄨ锛屽氨杩斿洖true
    }
};


class ScoreManage{
private:
    list<Student> m_stuList;                // 杩欎釜鎴愮哗璁板綍琛ㄩ噷璁板綍浜嗗摢浜涘鐢熺殑鐩稿叧淇℃伅
    string m_filePath = "F://cpp//ScoreManage//score.txt";



public:
    ifstream is;                            // 浠庢枃浠朵腑璇绘暟鎹�
    ofstream os;                            // 浠庢枃浠朵腑鍐欐暟鎹�
    int userSwitch;
    ScoreManage(){
        //F:\cpp\ScoreManage
        // C://Users//26541//Desktop//score.txt
        is.open(m_filePath);
        if(!is.is_open()){
            cout<<"瀛樺偍鏁版嵁鐨勬枃浠惰矾寰勬湁璇紒\n";
            system("pause");
        }else{
            is.seekg(0, ios::end);
            cout<<is.tellg()<<endl;
            int size = is.tellg();
            is.close();
            if(size == 0){
                cout<<"寰堟姳姝夋枃浠朵腑骞舵病鏈夊垵濮嬬殑瀛︾敓鏁版嵁锛乗n";
                system("pause");
            }else{
                readData();
            }


        }
    }
    void saveData(){
        string s;
        string stuName;
        string stuId;
        string courseName;
        string courseId;
        int score;
        int credit;
        os.open(m_filePath);
        if(os.is_open()){
            cout<<"鏂囦欢琚墦寮�浜嗭紒\n";
        }
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            stuName = it->getStuName();
            stuId = it->getStuId();
            for(list<Course>::iterator itt = it->getCourseList().begin(); itt != it->getCourseList().end(); itt++){
                courseName = itt->getCourseName();
                courseId = itt->getCourseId();
                score = itt->getScore();
                credit = itt->getCredits();
                s = stuName + '*' + stuId + '*' + courseName + '*' + courseId + '*' + to_string(score) + '*' + to_string(credit) + '\n';
                cout<<s<<endl;
                os.write(s.c_str(), s.size());
            }

        }
        os.close();
    }
    void readData(){
        string s;
        string stuName;
        string stuId;
        string courseName;
        string courseId;
        string score;
        string credit;
        is.open(m_filePath);
        while(getline(is, s)){
            cout<<s<<endl;
            vector<int> plot;
            for(int i = 0; i < s.size(); i++){
                if(s[i] == '*'){
                    plot.push_back(i);
                }
            }
            for(int i = 0; i < plot.size(); i++){
                cout<<plot[i]<<endl;
            }
            cout<<endl;
            stuName = s.substr(0,plot[0]);
            stuId = s.substr(plot[0] + 1, plot[1] - plot[0] - 1);
            courseName = s.substr(plot[1] + 1, plot[2] - plot[1] - 1);
            courseId = s.substr(plot[2] + 1, plot[3] - plot[2] - 1);
            score = s.substr(plot[3] + 1, plot[4] - plot[3] - 1);
            credit = s.substr(plot[4]+1, s.size() - plot[4] - 1);
            cout<<stuName<<endl;
            cout<<stuId<<endl;
            cout<<courseName<<endl;
            cout<<courseId<<endl;
            cout<<score<<endl;
            cout<<credit<<endl;
            int i_score = atoi(score.c_str());
            int i_credit = atoi(credit.c_str());
            addStudent(stuName, stuId,courseName, courseId, i_score, i_credit);
        }
        is.close();

        system("pause");
    }


    ~ScoreManage(){
        is.close();
        os.close();
    }

    bool isExistStu(const string& stuId){                   // 鍒ゆ柇璇ュ鐢熸槸鍚﹀凡缁忓瓨鍦�
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            if(it->getStuId() == stuId){
                return true;
            }
        }
        return false;
    }

    void inputAddInformation(string& stuName, string& stuId, string& courseName, string& courseId, int& score, int& credit){
        system("cls");
        bool flag = false;
        cout<<"璇疯緭鍏ュ鐢熷鍚�: ";cin>>stuName;
        cout<<"璇疯緭鍏ュ鐢烮D: ";cin>>stuId;
        cout<<"璇疯緭鍏ヨ绋嬪悕绉�: ";cin>>courseName;
        cout<<"璇疯緭鍏ヨ绋婭D: ";cin>>courseId;
        cout<<"璇疯緭鍏ヨ鍚嶅鐢熻璇剧▼鐨勬垚缁�: ";cin>>score;
        cout<<"璇疯緭鍏ヨ璇剧▼鐨勫鍒�: ";cin>>credit;
        if(!isStuIdValid(stuId)){
            flag = true;
            cout<<"瀛︾敓ID闀垮害蹇呴』涓�8!\n";
        }
        if(!isCourseIdValid(courseId)){
            cout<<"璇剧▼ID闀垮害蹇呴』涓�5!\n";
        }
        if(!isScoreValid(score)){
            cout<<"璇疯緭鍏ユ纭殑璇剧▼鎴愮哗(0-100)!\n";
        }
        if(!isCreditsValid(credit)){
            cout<<"璇疯緭鍏ユ纭殑璇剧▼瀛﹀垎(1-5)!\n";
        }
        if(flag){
            cout<<"There are some errors happened!\nPlease press any key to try it again!\n";
            system("pause");
            inputAddInformation(stuName, stuId, courseName, courseId, score, credit);

        }

    }
    void addStudent(){                              // 娣诲姞瀛︾敓鎴愮哗
        string stuName;
        string stuId;
        string courseName;
        string courseId;
        int score;
        int credit;
        inputAddInformation(stuName, stuId, courseName, courseId, score, credit);
        // 鐜板湪淇℃伅閮藉凡缁忚姝ｇ‘杈撳叆浜�
        Course course(courseId, courseName, credit, score);                // 鏋勫缓杩欓棬璇剧殑璇剧▼瀵硅薄
        if(isExistStu(stuId)){                      // 濡傛灉鍦ㄨ褰曡〃閲屽凡缁忔湁杩欎釜瀛︾敓浜�
            Student& stu = findStu(stuId);           // 鎵惧埌杩欎釜瀛︾敓
            if(!stu.isCourseValid(course.getCourseId())){               // 濡傛灉杩欎釜瀛︾敓宸茬粡鏈変簡杩欓棬璇剧殑鎴愮哗.
                // 璇存槑瑕侀噸鏂版墽琛宎ddStudent鍑芥暟浜�
                addStudent();                       // 閭ｅ氨閲嶅ご寮�濮嬫墽琛宎ddStudent杩欎釜鍑芥暟
            }else{                                  // 濡傛灉娌℃湁杩欓棬璇撅紝閭ｅ氨娣诲姞缁欏鐢�
                cout<<"宸茬粡鏈変簡杩欎釜瀛︾敓,鐩存帴缁欒繖涓鐢熸坊鍔犳垚缁‐n";
                stu.appendCourse(course);
            }
        }else{                                      // 濡傛灉璁板綍琛ㄩ噷娌℃湁杩欎釜瀛︾敓
            cout<<"娌℃湁杩欎釜瀛︾敓锛屾妸杩欎釜瀛︾敓娣诲姞鍒癿_stuList閲屽幓\n";
            Student stu(stuId, stuName, course);    // 鐩存帴鎶婅繖闂ㄨ娣诲姞缁欒繖涓鐢�
            m_stuList.push_back(stu);
        }
    }

    void addStudent(const string& stuName, const string& stuId, const string& courseName, const string& courseId, int score, int credit){
        Course course(courseId, courseName, credit, score);                // 鏋勫缓杩欓棬璇剧殑璇剧▼瀵硅薄
        if(isExistStu(stuId)){                      // 濡傛灉鍦ㄨ褰曡〃閲屽凡缁忔湁杩欎釜瀛︾敓浜�
            Student& stu = findStu(stuId);           // 鎵惧埌杩欎釜瀛︾敓
            if(!stu.isCourseValid(course.getCourseId())){               // 濡傛灉杩欎釜瀛︾敓宸茬粡鏈変簡杩欓棬璇剧殑鎴愮哗.
                // 璇存槑瑕侀噸鏂版墽琛宎ddStudent鍑芥暟浜�
                addStudent();                       // 閭ｅ氨閲嶅ご寮�濮嬫墽琛宎ddStudent杩欎釜鍑芥暟
            }else{                                  // 濡傛灉娌℃湁杩欓棬璇撅紝閭ｅ氨娣诲姞缁欏鐢�

                stu.appendCourse(course);
            }
        }else{                                      // 濡傛灉璁板綍琛ㄩ噷娌℃湁杩欎釜瀛︾敓
            Student stu(stuId, stuName, course);    // 鐩存帴鎶婅繖闂ㄨ娣诲姞缁欒繖涓鐢�
            m_stuList.push_back(stu);
        }
    }


    void delStu(){
        system("cls");
        string stuId;
        string courseId;
        cout<<"璇疯緭鍏ュ鐢烮D: ";cin>>stuId;
        cout<<"璇疯緭鍏ヨ鍒犻櫎鐨勮绋嬬殑ID: "; cin>>courseId;
        if(!isExistStu(stuId)){
            cout<<"寰堟姳姝夊綋鍓嶇郴缁熶腑娌℃湁璇ュ鐢熺殑淇℃伅!\n";
            cout<<"璇烽噸鏂拌緭鍏ユ纭殑瀛︾敓ID!\n";
            system("pause");
            delStu();
        }else{
            Student& stu = findStu(stuId);                   // 鎵惧埌杩欎釜瀛︾敓
            if(stu.isCourseValid(courseId)){               // 娌℃湁杩欓棬璇剧▼
                cout<<"寰堟姳姝夎瀛︾敓骞舵病鏈夎璇剧▼淇℃伅!\n";
                cout<<"璇烽噸鏂拌緭鍏�!\n";
                system("pause");
                delStu();
            }else{                                          // 鏈夎繖涓鐢燂紝涔熸湁杩欓棬璇剧▼
                stu.delCourse(courseId);                    // 鐩存帴鍒犻櫎鍗冲彲
            }
        }
    }
    void inputSearchInformation(string& stuId){
        system("cls");
        cout<<"璇疯緭鍏ュ鐢烮D: ";cin>>stuId;
        bool flag = false;
        if(!isStuIdValid(stuId)){
            cout<<"瀛︾敓ID杈撳叆涓嶈鑼�(闀垮害蹇呴』涓�8)!\n";
            flag = true;
        }
        if(flag){
            cout<<"寰堟姳姝夊嚭鐜颁竴浜涙剰澶杝!\n";
            cout<<"璇锋寜涓嬩换鎰忛敭閲嶈瘯涓�娆″惂~!\n";
            system("pause");
            inputSearchInformation(stuId);
        }
    }

    void showStu(){
        cout<<"褰撳墠鎴愮哗琛ㄤ腑鐨勫鐢熶俊鎭涓嬫墍绀篭n";
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            cout<<"瀛︾敓ID:"<<it->getStuId()<<"   ";
            cout<<"瀛︾敓濮撳悕锛�"<<it->getStuName()<<endl;
            cout<<"璇ュ鐢熼�夋嫨鐨勮绋嬫湁:\n";
            for(list<Course>::iterator itt = it->getCourseList().begin(); itt != it->getCourseList().end(); itt++){
                cout<<"璇剧▼ID:"<<itt->getCourseId()<<"   "<<"璇剧▼鍚嶇О:"<<itt->getCourseName()<<"   "<<"璇剧▼鍒嗘暟:"
                    <<itt->getScore()<<"璇剧▼瀛﹀垎:"<<itt->getCredits()<<endl;
            }
        }
    }
    void searchStuScore(){                                          // 鏌ユ壘鏌愪釜瀛︾敓鐨勫叏閮ㄨ绋嬫垚缁�
        string stuId;
        inputSearchInformation(stuId);                           // 杈撳叆姝ｇ‘鐨勫鐢熶俊鎭�
        if(!isExistStu(stuId)){                                     // 濡傛灉涓嶅瓨鍦ㄨ繖涓鐢燂紝灏辫繑鍥炰富鑿滃崟
            cout<<"褰撳墠鎴愮哗琛ㄩ噷娌℃湁璇ュ鐢烮D鐨勪俊鎭痋n";
            cout<<"鎸変换鎰忛敭杩斿洖涓昏彍鍗昞n";
            system("pause");
        }else{                                                      // 濡傛灉瀛樺湪杩欎釜瀛︾敓锛屽氨鏄剧ず瀛︾敓鐨勮绋嬫垚缁�
            Student stu = findStu(stuId);
            stu.showScore();
        }
    }
    void inputCourseInformation(string& courseId){
        system("cls");
        bool flag = false;
        cout<<"璇疯緭鍏ヨ绋婭D:";cin>>courseId;
        if(!isCourseIdValid(courseId)){
            cout<<"璇剧▼ID闀垮害蹇呴』涓�5\n";
            flag = true;
        }
        if(flag){
            cout<<"寰堟姳姝夊嚭鐜颁簡涓�浜涙剰澶朶n";
            cout<<"璇锋寜涓嬩换鎰忛敭閲嶈瘯涓�娆″惂~\n";
            system("pause");
        }
    }

    bool isExistCourse(const string& courseId){
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            for(list<Course>::iterator itt = (*it).getCourseList().begin(); itt != (*it).getCourseList().end(); itt++){
                if(itt->getCourseId() == courseId){
                    return true;
                }
            }
        }
        return false;
    }

    void statisCourse(const string& courseId){
        int totalNum = 0;                                       // 閫変簡璇ヨ绋嬬殑鎬讳汉鏁�
        int totalScore = 0;                                     // 閫変簡璇ヨ绋嬬殑鎬诲垎鏁�
        int failStu = 0;                                        // 璇ヨ绋嬫寕绉戠殑鎬讳汉鏁�
        string courseName;
        int cLevel = 0;
        int bLevel = 0;
        int aLevel = 0;
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            for(list<Course>::iterator itt = (*it).getCourseList().begin(); itt != (*it).getCourseList().end(); itt++){
                if(itt->getCourseId() == courseId){
                    courseName = itt->getCourseName();
                    totalNum++;
                    totalScore+=itt->getScore();
                    if(itt->getScore() < 60){
                        failStu++;
                        cLevel++;
                    }else if(itt->getScore() < 80){
                        bLevel++;
                    }else{
                        aLevel++;
                    }

                }
            }
        }
        cout<<"****************"<<courseName<<"********************"<<endl;
        cout<<"璇剧▼骞冲潎鎴愮哗涓�:"<<1.0*totalScore/totalNum<<endl;
        cout<<"璇剧▼閫氳繃鐜囦负:"<<1.0*(totalNum - failStu)/totalNum<<endl;
        cout<<"璇剧▼璇勭骇涓篊(涓嶅強鏍�)浜烘暟鍗犳瘮锛�"<<1.0*(cLevel)/totalNum<<endl;
        cout<<"璇剧▼璇勭骇涓築(鑹�)浜烘暟鍗犳瘮锛�"<<1.0*(bLevel)/totalNum<<endl;
        cout<<"璇剧▼璇勭骇涓篈(浼�)浜烘暟鍗犳瘮锛�"<<1.0*(aLevel)/totalNum<<endl;
    }
    void searchCourseScore(){
        string courseId;
        inputCourseInformation(courseId);
        statisCourse(courseId);
    }

    Student& findStu(const string& stuId){
        for(list<Student>::iterator it = m_stuList.begin(); it != m_stuList.end(); it++){
            if(it->getStuId() == stuId){
                return *it;
            }
        }
    }
    bool isStuIdValid(string stuId){
        return stuId.size() == 8;
    }
    bool isCourseIdValid(string courseId){
        return courseId.size() == 5;
    }
    bool isCreditsValid(int credits){
        if(credits < 1 || credits > 5){
            return false;
        }else{
            return true;
        }
    }
    bool isScoreValid(int score){
        if(score < 0 || score > 100){
            return false;
        }else{
            return true;
        }
    }



    void showMenu(){
        cout<<"                      鎴愮哗璁板綍琛�                   \n\n\n\n";
        cout<<"1.娣诲姞瀛︾敓璇剧▼鎴愮哗\n";
        cout<<"2.鍒犻櫎瀛︾敓璇剧▼鎴愮哗\n";
        cout<<"3.鏌ヨ瀛︾敓鎴愮哗\n";
        cout<<"4.鏌ヨ璇剧▼鎴愮哗\n";
        cout<<"5.鍐欏叆鏂囦欢\n";
        cout<<"6.璇诲彇鏂囦欢\n";
        cout<<"0.閫�鍑篭n";
    }


};


int main() {
    ScoreManage scoreManage;
    scoreManage.showMenu();
    bool flag = false;
    while(1){
        system("cls");
        scoreManage.showMenu();
        cin>>scoreManage.userSwitch;
        switch(scoreManage.userSwitch){
            case 0:                         // 閫�鍑�
                flag = true;
                break;
            case 1:                         // 娣诲姞
                scoreManage.addStudent();
                cout<<"瀛︾敓淇℃伅娣诲姞瀹屾瘯\n";
                system("pause");
                break;
            case 2:                         // 鍒犻櫎
                scoreManage.delStu();
                cout<<"瀛︾敓淇℃伅宸插垹闄n";
                system("pause");
                break;
            case 3:                         // 鏌ュ鐢熸垚缁�
                scoreManage.searchStuScore();
                system("pause");
                break;
            case 4:                         // 鏌ョ彮绾ф垚缁�
                scoreManage.searchCourseScore();
                system("pause");
                break;
            case 5:                         // 鍐欏叆鏂囦欢
                scoreManage.saveData();
                cout<<"璇锋寜浠绘剰閿洖鍒颁富鑿滃崟\n";
                system("pause");
                break;
            case 6:                         // 璇诲彇鏂囦欢
                cout<<"璇锋寜浠绘剰閿洖鍒颁富鑿滃崟\n";
                system("pause");
                break;
            default:
                scoreManage.showMenu();
                break;
        }
        if(flag){
            break;
        }

    }
    return 0;
}
